package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ILigneCommandeClientDao;
import com.stock.mvc.entites.LigneCommandeClient;
import com.stock.mvc.services.ILigneCommandeClientService;
 @Transactional 
public class LigneCommandeClientServiceImpl implements ILigneCommandeClientService{
	 private ILigneCommandeClientDao dao;
	   

	public ILigneCommandeClientDao getDao() {
		return dao;
	}

	public void setDao(ILigneCommandeClientDao dao) {
		this.dao = dao;
	}

	public LigneCommandeClient save(LigneCommandeClient entity) {
	
		return  dao.save(entity) ;
	}

	
	public LigneCommandeClient update(LigneCommandeClient entity) {
		
		return dao.update(entity);
	}


	public List<LigneCommandeClient> selectAll() {
		
		return dao.selectAll();
	}

	
	public List<LigneCommandeClient> selectAll(String sortField, String sort) {
	
		return dao.selectAll(sortField, sort);
	}


	public LigneCommandeClient getById(Long id) {
		 
		return dao.getById(id);
	}

	
	
	public void remove(Long id) {
		dao.remove(id);
		
		
	}


	public LigneCommandeClient findOne(String paramName, Object paramValue) {
		
		return dao.findOne(paramName, paramValue);
	}

	
	public LigneCommandeClient findOne(String[] paramNames, Object[] paramValues) {
	
		return dao.findOne(paramNames, paramValues);
	}

	
	public int findCountBy(String paramName, String paramValue) {
		
		return dao.findCountBy(paramName, paramValue);
	}

}
